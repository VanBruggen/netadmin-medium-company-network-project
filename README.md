# A wireless network project for a medium company

Language: Polish.

A PDF with a thorough project of a wireless network for a hypothetical, medium company. Schematics, protocols, signal strength calculations, hardware, assembly, cost.

A part of the _Wireless Networks_ subject on my CS studies. Officialy a two-person job, but I've done almost everything myself.